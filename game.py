from random import randint

name = input("Hi! What is your name? ")

for num_guess in range(1, 6):
    print("Guess", num_guess, ":", name, "were you born in", randint(1, 12),"/",randint(1924, 2004),"?")
    answer = input("yes or no? ")
    if answer == "yes":
        print("I knew it!")
        exit()
    elif num_guess == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! lemme try again!")
